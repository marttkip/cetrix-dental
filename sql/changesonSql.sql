create or replace view v_visit_invoices AS select sum((`visit_charge`.`visit_charge_amount` * `visit_charge`.`visit_charge_units`)) AS `total_invoice`,`visit_charge`.`visit_id` AS `visit_id` from `visit_charge` where ((`visit_charge`.`charged` = 1) and (`visit_charge`.`visit_charge_delete` = 0) and (`visit_charge`.`service_charge_id` <> 20962)) group by `visit_charge`.`visit_id`;


create or replace view v_visit_payments AS  select sum(`payments`.`amount_paid`) AS `amount_paid`,`payments`.`visit_id` AS `visit_id` from `payments` where ((`payments`.`cancel` = 0) and (`payments`.`payment_type` = 1)) group by `payments`.`visit_id`;

create or replace view v_waivers AS  select sum(`payments`.`amount_paid`) AS `total_waiver`,`payments`.`visit_id` AS `visit_id` from `payments` where ((`payments`.`cancel` = 0) and (`payments`.`payment_type` = 2)) group by `payments`.`visit_id`;
CREATE OR REPLACE view v_patient_invoices AS select sum((`visit_charge`.`visit_charge_amount` * `visit_charge`.`visit_charge_units`)) AS `total_invoice`,`patients`.`patient_id` AS `patient_id` from (`patients` join (`visit` left join `visit_charge` on((`visit_charge`.`visit_id` = `visit`.`visit_id`)))) where ((`visit`.`patient_id` = `patients`.`patient_id`) and (`visit_charge`.`charged` = 1) and (`visit_charge`.`visit_charge_delete` = 0)) group by `patients`.`patient_id`;
CREATE OR REPLACE view v_patient_payments AS select sum(`payments`.`amount_paid`) AS `amount_paid`,`patients`.`patient_id` AS `patient_id` from (`patients` join (`visit` left join `payments` on((`payments`.`visit_id` = `visit`.`visit_id`)))) where ((`visit`.`patient_id` = `patients`.`patient_id`) and (`payments`.`cancel` = 0) and (`payments`.`payment_type` = 1)) group by `patients`.`patient_id`;
CREATE OR REPLACE VIEW v_patient_waivers AS select sum(`payments`.`amount_paid`) AS `total_waiver`,`patients`.`patient_id` AS `patient_id` from (`patients` join (`visit` left join `payments` on((`payments`.`visit_id` = `visit`.`visit_id`)))) where ((`visit`.`patient_id` = `patients`.`patient_id`) and (`payments`.`cancel` = 0) and (`payments`.`payment_type` = 3)) group by `patients`.`patient_id`;
CREATE OR REPLACE view v_patient_statement  AS select `patients`.`patient_id` AS `patient_id`,ifnull(`v_patient_invoices`.`total_invoice`,0) AS `total_invoice_amount`,ifnull(`v_patient_payments`.`amount_paid`,0) AS `total_paid_amount`,ifnull(`v_patient_waivers`.`total_waiver`,0) AS `total_waived_amount` from (((`patients` left join `v_patient_invoices` on((`v_patient_invoices`.`patient_id` = `patients`.`patient_id`))) left join `v_patient_waivers` on((`v_patient_waivers`.`patient_id` = `patients`.`patient_id`))) left join `v_patient_payments` on((`v_patient_payments`.`patient_id` = `patients`.`patient_id`)));
create or replace view v_patient_balances AS select `v_patient_statement`.`patient_id` AS `patient_id`,`v_patient_statement`.`total_invoice_amount` AS `total_invoice_amount`,`v_patient_statement`.`total_paid_amount` AS `total_paid_amount`,`v_patient_statement`.`total_waived_amount` AS `total_waived_amount`,((`v_patient_statement`.`total_invoice_amount` - `v_patient_statement`.`total_paid_amount`) - `v_patient_statement`.`total_waived_amount`) AS `balance` from `v_patient_statement`;
create or replace view  v_service_product_amount AS select sum((`visit_charge`.`visit_charge_units` * ((`service_charge`.`service_charge_amount` * 100) / 133))) AS `service_total`,`visit`.`visit_date` AS `visit_date`,`service`.`service_id` AS `service_id` from (((`visit_charge` join `service_charge`) join `service`) join `visit`) where ((`visit_charge`.`visit_charge_delete` = 0) and (`visit_charge`.`service_charge_id` = `service_charge`.`service_charge_id`) and (`service`.`service_id` = `service_charge`.`service_id`) and (`visit`.`visit_id` = `visit_charge`.`visit_id`) and (`visit`.`visit_delete` = 0) and (`service`.`service_status` = 1)) group by `visit`.`visit_date`;
create or replace VIEW v_department_invoices AS select sum((`visit_charge`.`visit_charge_units` * `visit_charge`.`visit_charge_amount`)) AS `service_total`,`visit`.`visit_date` AS `visit_date`,`service`.`service_id` AS `service_id`,`departments`.`department_id` AS `department_id` from ((((`visit_charge` join `service_charge`) join `service`) join `visit`) join `departments`) where ((`visit_charge`.`visit_charge_delete` = 0) and (`visit`.`visit_id` = `visit_charge`.`visit_id`) and (`visit`.`visit_delete` = 0) and (`visit_charge`.`service_charge_id` = `service_charge`.`service_charge_id`) and (`departments`.`department_id` = `service`.`department_id`) and (`service`.`service_id` = `service_charge`.`service_id`) and (`service`.`service_status` = 1)) group by `visit`.`visit_date`;
CREATE OR REPLACE VIEW v_patient_visit_statement AS
SELECT `visit`.`visit_id`, `visit`.`close_card`, `patients`.`patient_id`, `patients`.`patient_othernames`, `patients`.`patient_surname`, `visit_type`.`visit_type_name`, `personnel`.`personnel_fname`, `personnel`.`personnel_onames`, `visit`.`hold_card`, `visit`.`invoice_number`, `visit`.`visit_date`,
`visit`.`visit_type`, SUM(v_visit_invoices.total_invoice) AS total_invoice,
SUM(IFNULL(v_visit_payments.amount_paid,0)) AS total_amount_paid,
SUM(IFNULL(v_waivers.total_waiver,0)) AS total_waiver,
SUM(v_visit_invoices.total_invoice) - SUM(IFNULL(v_visit_payments.amount_paid,0)) - SUM(IFNULL(v_waivers.total_waiver,0)) AS balance
FROM (`visit`, `patients`, `visit_type`)
LEFT JOIN `personnel` ON `visit`.`personnel_id` = `personnel`.`personnel_id`
LEFT JOIN `v_visit_invoices` ON `visit`.`visit_id` = `v_visit_invoices`.`visit_id`
LEFT JOIN `v_visit_payments` ON `visit`.`visit_id` = `v_visit_payments`.`visit_id`
LEFT JOIN `v_waivers` ON `visit`.`visit_id` = `v_waivers`.`visit_id`
WHERE `visit`.`patient_id` = patients.patient_id
AND visit_type.visit_type_id = visit.visit_type
AND visit.visit_delete = 0
AND patients.patient_type = 0
AND visit.close_card < 5
GROUP BY `visit`.`visit_id`
ORDER BY `visit`.`visit_id`;
-- Addition of Account Id in the service Table
-- Command 1
ALTER TABLE `service` ADD `account_id` INT(50) NOT NULL AFTER `service_id`;

-- Command 2
ALTER TABLE `payment_method` ADD `account_id` INT(50) NOT NULL AFTER `payment_method`;
ALTER TABLE `orders` ADD `account_id` INT(50) NOT NULL AFTER `deduction_type_id`;
ALTER TABLE `orders` ADD `reference_id` INT(50) NOT NULL AFTER `account_id`;
ALTER TABLE `finance_purchase` ADD `department_id` INT(50) NOT NULL AFTER `creditor_id`;
ALTER TABLE `account` ADD `paying_account` INT(50) NOT NULL AFTER `inventory_status`;
