<!-- search -->
<?php echo $this->load->view('search/search_walkins', '', TRUE);?>
<!-- end search -->
 
 <section class="panel panel-success ">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?> for <?php echo date('jS M Y',strtotime(date('Y-m-d')));?></h2>

        <div class="pull-right">
	         <a class='btn btn-sm btn-warning ' data-toggle='modal' data-target='#add_pharmacy'  style="margin-top:-35px"> <i class="fa fa-plus"></i> New Customer </a>        
	         
	    </div>
    </header>
      <div class="panel-body">
          <div class="padd">

          	<div class="modal fade bs-example-modal-lg" id="add_pharmacy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			    <div class="modal-dialog modal-lg" role="document">
			        <div class="modal-content">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			                <h4 class="modal-title" id="myModalLabel">Add New Customer</h4>
			            </div>
			            <?php echo form_open("create-walkin-visit1/5", array("class" => "form-horizontal"));?>
			            <div class="modal-body">
			            	<div class="row">
			                	<div class='col-md-12'>
			                      	<div class="form-group">
										<label class="col-lg-4 control-label">Patient Name: </label>
										<div class="col-lg-8">
											<input type="text" class="form-control" name="patient_name" placeholder="" autocomplete="off">
										</div>
									  
									</div>
									 <input type="hidden" class="form-control" name="redirect_url" placeholder="" autocomplete="off" value="<?php echo $this->uri->uri_string()?>">
			                      	<div class="form-group" style="display: none">
										<label class="col-lg-4 control-label"> Patient Gender: </label>
									  
										
										<div class="col-lg-8">
											<select id='parent_service_id' name='gender' class='form-control custom-select ' >
						                      <option value='0'>None - Please gender</option>
						                       <option value='1'>Male</option>
						                       <option value='2'>Female</option>

						                    </select>
										</div>
									</div>				
			                      	<div class="form-group" style="display: none">
										<label class="col-lg-4 control-label">Age: </label>
									  
										<div class="col-lg-8">
											<input type="text" class="form-control" name="age" placeholder="" autocomplete="off" >
										</div>
									</div>
			                    </div>
			                </div>
			            </div>
			            <div class="modal-footer">
			            	<button type="submit" class='btn btn-info btn-sm' type='submit' >Add Patient</button>
			                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			                
			            </div>
			            <?php echo form_close();?>
			        </div>
			    </div>
			</div>

			

<?php
		$search = $this->session->userdata('walkin_search');
		
		if(!empty($search))
		{
			echo '<a href="'.site_url().'reception/close_walkin_search" class="btn btn-warning">Close Search</a>';
		}
		$result = '';
		$dependant_id =0;
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
				
			
			
			$result .= 
				'
					<table class="table  table-bordered ">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Patient Names</th>
						  <th>Visit Type</th>
						  <th>Coming From</th>
						  <th>Going To</th>
						  <th>Sent At</th>
						  <th colspan="6">Actions</th>
						</tr>
					  </thead>
					  <tbody>
				';
			
			// $personnel_query = $this->personnel_model->get_all_personnel();
			
			foreach ($query->result() as $row)
			{
				$visit_date = date('jS M Y',strtotime($row->visit_date));
				$visit_time = date('H:i a',strtotime($row->visit_time));
				if($row->visit_time_out != '0000-00-00 00:00:00')
				{
					$visit_time_out = date('H:i a',strtotime($row->visit_time_out));
				}
				else
				{
					$visit_time_out = '-';
				}
				$visit_created = date('H:i a',strtotime($row->visit_date));
				$visit_id = $row->visit_id;
				$patient_id = $row->patient_id;
				$personnel_id = $row->personnel_id;
				$dependant_id = $row->dependant_id;
				$patient_number = $row->patient_number;
				$strath_no = $row->strath_no;
				$visit_type_id = $row->visit_type_id;
				$visit_type = $row->visit_type;
				$accounts = "";//$row->accounts;
				$visit_table_visit_type = $visit_type;
				$patient_table_visit_type = $visit_type_id;
				$coming_from = $this->reception_model->coming_from($visit_id);
				$sent_to = $this->reception_model->going_to($visit_id);

				$visit_type_name = $row->visit_type_name;
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_date_of_birth = $row->patient_date_of_birth;
				$hold_card = $row->hold_card;
				$ward_id = 1;//$row->ward_id;




				if($coming_from == "Laboratory")
				{
					$close_page = 6;
				}
				else{
					$close_page = 5;
				}
				



				//cash paying patient sent to department but has to pass through the accounts
				if($coming_from == "Laboratory")
				{
					$balanced = 'info';
				}
				else
				{
					$balanced = 'default';
				}
				
				//creators and editors
				// if($personnel_query->num_rows() > 0)
				// {
				// 	$personnel_result = $personnel_query->result();
					
				// 	foreach($personnel_result as $adm)
				// 	{
				// 		$personnel_id2 = $adm->personnel_id;
						
				// 		if($personnel_id == $personnel_id2)
				// 		{
				// 			$doctor = $adm->personnel_onames;
				// 			break;
				// 		}
						
				// 		else
				// 		{
				// 			$doctor = '-';
				// 		}
				// 	}
				// }
				
				// else
				// {
				// 	$doctor = '-';
				// }
				$v_data = array('visit_id'=>$visit_id);
				$count++;
				

				

				$close_page = 0;
				
				$buttons ='<td><a href="'.site_url().'receipt-payment/'.$visit_id.'/'.$close_page.'" class="btn btn-sm btn-info" >Payments</a></td>';

				
				

								
					
				$result .= 
					'
						<tr class="'.$balanced.'">
							<td>'.$count.'</td>
							<td>'.$patient_surname.' '.$patient_othernames.'</td>
							<td>'.$visit_type_name.'</td>
							<td>'.$coming_from.'</td>
							<td>'.$sent_to.'</td>
							<td>'.$visit_time.'</td>
							'.$buttons.'
						</tr> 
					';
					
					$v_data['patient_type'] = $visit_type_id;
				
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no patients";
		}
		
?>
		<?php
		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger">'.$error.'</div>';
			$this->session->unset_userdata('error_message');
		}
		
		if(!empty($success))
		{
			echo '<div class="alert alert-success">'.$success.'</div>';
			$this->session->unset_userdata('success_message');
		}
		echo $result;
		?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        </div>
        <!-- Widget ends -->
       

  </section>

  <audio id="sound1" src="<?php echo base_url();?>sound/nothing/beep.mp3"></audio>
  <script type="text/javascript">
  	$(document).ready(function(){
  	   $("#personnel_id").customselect();
       $("#bed_id").customselect();
       $("#room_id").customselect();
       var department_id = document.getElementById("department_id").value;
       // alert(department_id);
		setInterval(function(){check_new_patients(department_id)},10000);

	 });

  
   	function check_new_patients(module)
		{	
		 var XMLHttpRequestObject = false;
        
		    if (window.XMLHttpRequest) {
		    
		        XMLHttpRequestObject = new XMLHttpRequest();
		    } 
		        
		    else if (window.ActiveXObject) {
		        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		    
		    var config_url = $('#config_url').val();
		    var url = config_url+"nurse/check_queues/"+module;
		    // alert(url);
		    if(XMLHttpRequestObject) {
		                
		        XMLHttpRequestObject.open("GET", url);
		                
		        XMLHttpRequestObject.onreadystatechange = function(){
		            
		            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
		            	
	         			var one = XMLHttpRequestObject.responseText;
	         			if(one == 1)
	         			{
	         				 var audio1 = document.getElementById("sound1");
						 	 if (audio1.paused !== true){
							    audio1.pause();
							 }
							 else
							 {
								audio1.play();
							 }
	         			}
	         			else
	         			{

	         			}
			         	
	         
		            }
		        }
		                
		        XMLHttpRequestObject.send(null);
		    }
		}

	function get_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = '';
		button.style.display = 'none';
		button2.style.display = '';
	}
	function close_visit_trail(visit_id){

		var myTarget2 = document.getElementById("visit_trail"+visit_id);
		var button = document.getElementById("open_visit"+visit_id);
		var button2 = document.getElementById("close_visit"+visit_id);

		myTarget2.style.display = 'none';
		button.style.display = '';
		button2.style.display = 'none';
	}
  </script>

  <script type="text/javascript">
	
	$(document).on("change","select#ward_id",function(e)
	{
		var ward_id = $(this).val();
		
		var url = "<?php echo site_url();?>nurse/get_ward_rooms/"+ward_id;
		// alert(url);
		//get rooms
		$.get( url , function( data ) 
		{
			$( "#room_id" ).html( data );
			
			$.get( "<?php echo site_url();?>nurse/get_room_beds/0", function( data ) 
			{
				$( "#bed_id" ).html( data );
			});
		});
	});
	
	$(document).on("change","select#room_id",function(e)
	{
		var room_id = $(this).val();
		
		//get beds
		$.get( "<?php echo site_url();?>nurse/get_room_beds/"+room_id, function( data ) 
		{
			$( "#bed_id" ).html( data );
		});
	});
</script>

 