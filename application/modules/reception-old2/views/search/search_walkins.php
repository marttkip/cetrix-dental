 <section class="panel panel-success">
    <header class="panel-heading">
        <h2 class="panel-title">Search <?php echo $title?> for <?php echo date('jS M Y',strtotime(date('Y-m-d'))); ?></h2>
    </header>
    <!-- Widget content -->
         <div class="panel-body">
    	<div class="padd">
			<?php
			
			
			echo form_open("reception/search_walkin", array("class" => "form-horizontal"));
			
            
            ?>
            <div class="row">
                
                
                <div class="col-md-6 col-md-offset-3">
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label">Patient Name: </label>
                        
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="patient_name" placeholder="Name">
                        </div>
                    </div>
                    
                    
                    
                </div>
                
            </div>
            <br>
            <div class="row">
            	<div class="col-md-12">
                	<div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="center-align">
                                <button type="submit" class="btn btn-info">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
</section>