<?php

class Notes_formats_model extends CI_Model 
{	
	/*
	*	Retrieve all notes_formats
	*
	*/
	public function all_notes_formats()
	{
		$this->db->where(array('note_format_status' => 1, 'branch_code' => $this->session->userdata('branch_code')));
		$this->db->order_by('note_format_name');
		$query = $this->db->get('notes_formats');
		
		return $query;
	}
	
	/*
	*	Retrieve all notes_formats
	*	@param string $table
	* 	@param string $where
	*
	*/
	public function get_all_notes_formats($table, $where, $per_page, $page, $order = 'note_format_name', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}

	public function get_all_notes_formats_account($table, $where, $per_page, $page, $order = 'note_format_account_id', $order_method = 'ASC')
	{
		//retrieve all users
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $per_page, $page);
		
		return $query;
	}
	
	/*
	*	Add a new note_format
	*	@param string $image_name
	*
	*/
	public function add_note_format()
	{
		$data = array(
				'note_format_name'=>$this->input->post('note_format_name'),
				'note_format_status'=>$this->input->post('note_format_status'),
				'created'=>date('Y-m-d H:i:s'),
				'created_by'=>$this->session->userdata('personnel_id'),
				'branch_code'=>$this->session->userdata('branch_code'),
				'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('notes_formats', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Update an existing note_format
	*	@param string $image_name
	*	@param int $note_format_id
	*
	*/
	public function update_note_format($note_format_id)
	{
		$data = array(
				'note_format_name'=>$this->input->post('note_format_name'),
				'note_format_status'=>$this->input->post('note_format_status'),
				'branch_code'=>$this->session->userdata('branch_code'),
				'modified_by'=>$this->session->userdata('personnel_id')
			);
			
		$this->db->where('note_format_id', $note_format_id);
		if($this->db->update('notes_formats', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function add_account_to_note_format($note_format_id)
	{
		# code..

		$data = array(
				'account_id'=>$this->input->post('account_id'),
				'note_format_account_status'=>1,
				'note_format_id'=>$note_format_id,
				'created'=>date('Y-m-d'),
				'created_by'=>$this->session->userdata('personnel_id')
			);
			
		if($this->db->insert('note_format_account', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	get a single note_format's details
	*	@param int $note_format_id
	*
	*/
	public function get_note_format($note_format_id)
	{
		//retrieve all users
		$this->db->from('notes_formats');
		$this->db->select('*');
		$this->db->where('note_format_id = '.$note_format_id);
		$query = $this->db->get();
		
		return $query;
	}
	
	/*
	*	Delete an existing note_format
	*	@param int $note_format_id
	*
	*/
	public function delete_note_format($note_format_id)
	{
		if($this->db->delete('notes_formats', array('note_format_id' => $note_format_id)))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Activate a deactivated note_format
	*	@param int $note_format_id
	*
	*/
	public function activate_note_format($note_format_id)
	{
		$data = array(
				'note_format_status' => 1
			);
		$this->db->where('note_format_id', $note_format_id);
		
		if($this->db->update('notes_formats', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	/*
	*	Deactivate an activated note_format
	*	@param int $note_format_id
	*
	*/
	public function deactivate_note_format($note_format_id)
	{
		$data = array(
				'note_format_status' => 0
			);
		$this->db->where('note_format_id', $note_format_id);
		
		if($this->db->update('notes_formats', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
}
?>