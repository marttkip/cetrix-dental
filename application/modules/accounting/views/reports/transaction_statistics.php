<div class="row statistics">
    
    <div class="col-md-12 col-sm-12">
         <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
                <h2 class="panel-title">Transaction breakdown for <?php echo $total_visits;?> patients</h2>
            </header>             
        
              <!-- Widget content -->
              <div class="panel-body">
                <div class="row">
                   
                    <div class="col-md-3">
                        <?php
                        
                            $total_invoices_revenue = $this->accounting_model->get_visit_invoice_totals();
                            $total_invoices_revenue2 = $this->accounting_model->get_visit_invoice_totals2();
                            $total_transfers = $this->accounting_model->get_visit_invoice_children_totals();
                            $total_payments_revenue = $this->accounting_model->get_visit_payment_totals();
                            $total_payments_revenue_insurance = $this->accounting_model->get_visit_payment_totals_insurance();
                            $total_payments_revenue2 = $this->accounting_model->get_visit_payment_totals2();
                            $total_payments_revenue_current = $this->accounting_model->get_visit_payment_totals_current();
                            $total_waiver_revenue = $this->accounting_model->get_visit_waiver_totals();
                            $total_debits_revenue = $this->accounting_model->get_visit_debits_totals();
                      
                            $all_payments_period = $this->accounting_model->all_payments_period();
                            $receivable = $all_payments_period - $total_payments_revenue;
                            $total_rejected_amounts = $this->accounting_model->get_rejected_amounts();
                            


                            $all_properties = $this->accounting_model->get_all_visit_types();
                            $receivables = '';
                            $total_balances = 0;
                           if($all_properties->num_rows() > 0)
                            {

                           foreach ($all_properties->result() as $key => $value) {
        // code...
                          $visit_type_id = $value->visit_type_id;
                          $visit_type_name = $value->visit_type_name;
                          $property_balance = $this->accounting_model->get_receivable_balances($visit_type_id);
                          $receivables .= '   <tr>
                                                    <td class="text-left">'.strtoupper($visit_type_name).'</td>
                                            <td class="text-right"><a href="'.site_url().'customer-invoices/'.$visit_type_id.'" >'.number_format($property_balance,2).'</a></td>
                                        </tr>';
                          $total_balances += $property_balance;
                          }


                         }
                        $receivables .= '    <tr>
                                            <td class="text-left"><b>Total Payments</b></td>
                                    <td class="text-right"><b class="match">'.number_format($total_balances,2).'</b></td>
                                </tr>';

                            // $total_invoices_revenue -= $total_rejected_amounts;


                            // get payments done today and visits not for today
                            // var_dump($total_payments_revenue); die();
                            $cash_debt_repayments = $this->accounting_model->get_all_visit_payments_totals(1,1);
                            $insurance_debt_repayments = $this->accounting_model->get_all_visit_payments_totals(2,1);




                            if($receivable < 0)
                            {
                                $receivable = ($receivable);
                            }

                        ?>
                        <?php
                        $patients = $this->accounting_model->get_patients_visits(1);
                        $patients1 = $this->accounting_model->get_patients_visits_z(1);
                        $patients2 = $this->accounting_model->get_patients_visits_b(1);
                        $patients3 = $this->accounting_model->get_patients_visits_m(1);
                        $patients4 = $this->accounting_model->get_patients_visits_t(1);
                        $patients5 = $this->accounting_model->get_patients_visits_k(1);
                        $patients6 = $this->accounting_model->get_patients_visits_r(1);
                        $patients7 = $this->accounting_model->get_patients_visits_i(1);
                        $patients8 = $this->accounting_model->get_patients_visits_g(1);
                       // $patients = $this->accounting_model->get_patients_visits(1);
                        // $returning_patients = $this->accounting_model->get_patients_visits(0);
                        ?>
                        <h5>VISIT TYPE BREAKDOWN</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                 <tr>
                                    <th>TOTAL INVOICES</th>
                                    <td><?php echo number_format($total_invoices_revenue + $total_invoices_revenue2, 2);?></td>
                                </tr>
                                
                               <tr>
                                    <th>TOTAL DEBITS</th>
                                    <td><?php echo number_format($total_debits_revenue, 2);?></td>
                                </tr>
                                <tr>
                                    <th>TOTAL COLLECTION</th>
                                    <td>(<?php echo number_format($total_payments_revenue + $total_payments_revenue_current -$total_waiver_revenue, 2);?>)</td>
                                </tr>
                                 <tr>
                                    <th>TOTAL WAIVERS</th>
                                    <td>(<?php echo number_format($total_waiver_revenue, 2);?>)</td>
                                </tr>

                                <tr>
                                    <th>DEBT BALANCE</th>
                                    <td><?php echo number_format(($total_invoices_revenue +$total_debits_revenue + $total_invoices_revenue2 - $total_payments_revenue - $total_payments_revenue2 - $total_waiver_revenue), 2);?></td>
                                </tr>
                            </tbody>
                        </table>
                   
                    

                         <h5>PATIENT SEEN BY DOCTOR PAUL KIARIE</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                              <!--   <tr>
                                    <th>NEW PATIENTS</th>
                                    <td><?php echo $patients['total_new'];?></td>
                                </tr>
                                <tr>
                                    <th>RETURNING PATIENTS</th>
                                    <td><?php echo $patients['total_old'];?></td>
                                </tr> -->
                                 <tr>
                                    <th>TOTAL PATIENTS</th>
                                    <td><?php echo $patients['total_new'] + $patients['total_old'];?></td>
                                </tr>
                            </tbody>
                        </table>
                         <h5>PATIENT SEEN BY DOCTOR MULUPI EVELYNE</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                              <!--   <tr>
                                    <th>NEW PATIENTS</th>
                                    <td><?php echo $patients['total_new'];?></td>
                                </tr>
                                <tr>
                                    <th>RETURNING PATIENTS</th>
                                    <td><?php echo $patients['total_old'];?></td>
                                </tr> -->
                                 <tr>
                                    <th>TOTAL PATIENTS</th>
                                    <td><?php echo $patients1['total_new'] + $patients1['total_old'];?></td>
                                </tr>
                            </tbody>
                        </table>

                           <h5>PATIENT SEEN BY DOCTOR KOECH LIONEL</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                              <!--   <tr>
                                    <th>NEW PATIENTS</th>
                                    <td><?php echo $patients['total_new'];?></td>
                                </tr>
                                <tr>
                                    <th>RETURNING PATIENTS</th>
                                    <td><?php echo $patients['total_old'];?></td>
                                </tr> -->
                                 <tr>
                                    <th>TOTAL PATIENTS</th>
                                    <td><?php echo $patients2['total_new'] + $patients2['total_old'];?></td>
                                </tr>
                            </tbody>
                        </table>


                           <h5>PATIENT SEEN BY DOCTOR CHERUYIOT DENNIS</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                              <!--   <tr>
                                    <th>NEW PATIENTS</th>
                                    <td><?php echo $patients['total_new'];?></td>
                                </tr>
                                <tr>
                                    <th>RETURNING PATIENTS</th>
                                    <td><?php echo $patients['total_old'];?></td>
                                </tr> -->
                                 <tr>
                                    <th>TOTAL PATIENTS</th>
                                    <td><?php echo $patients3['total_new'] + $patients3['total_old'];?></td>
                                </tr>
                            </tbody>
                        </table>

                          <h5>PATIENT SEEN BY DOCTOR LUKANDU OCHIBA</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                              <!--   <tr>
                                    <th>NEW PATIENTS</th>
                                    <td><?php echo $patients['total_new'];?></td>
                                </tr>
                                <tr>
                                    <th>RETURNING PATIENTS</th>
                                    <td><?php echo $patients['total_old'];?></td>
                                </tr> -->
                                 <tr>
                                    <th>TOTAL PATIENTS</th>
                                    <td><?php echo $patients4['total_new'] + $patients4['total_old'];?></td>
                                </tr>
                            </tbody>
                        </table>

                           <h5>PATIENT SEEN BY DOCTOR MICHA CYRUS</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                              <!--   <tr>
                                    <th>NEW PATIENTS</th>
                                    <td><?php echo $patients['total_new'];?></td>
                                </tr>
                                <tr>
                                    <th>RETURNING PATIENTS</th>
                                    <td><?php echo $patients['total_old'];?></td>
                                </tr> -->
                                 <tr>
                                    <th>TOTAL PATIENTS</th>
                                    <td><?php echo $patients5['total_new'] + $patients5['total_old'];?></td>
                                </tr>
                            </tbody>
                        </table>
                              <h5>PATIENT SEEN BY DOCTOR DIANA ROP</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                              <!--   <tr>
                                    <th>NEW PATIENTS</th>
                                    <td><?php echo $patients['total_new'];?></td>
                                </tr>
                                <tr>
                                    <th>RETURNING PATIENTS</th>
                                    <td><?php echo $patients['total_old'];?></td>
                                </tr> -->
                                 <tr>
                                    <th>TOTAL PATIENTS</th>
                                    <td><?php echo $patients6['total_new'] + $patients6['total_old'];?></td>
                                </tr>
                            </tbody>
                        </table>
                             <h5>PATIENT SEEN BY DOCTOR BENJAMIN INDUSWE</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                              <!--   <tr>
                                    <th>NEW PATIENTS</th>
                                    <td><?php echo $patients['total_new'];?></td>
                                </tr>
                                <tr>
                                    <th>RETURNING PATIENTS</th>
                                    <td><?php echo $patients['total_old'];?></td>
                                </tr> -->
                                 <tr>
                                    <th>TOTAL PATIENTS</th>
                                    <td><?php echo $patients7['total_new'] + $patients7['total_old'];?></td>
                                </tr>
                            </tbody>
                        </table>
                            <h5>PATIENT SEEN BY DOCTOR MWAI GEORGE</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                              <!--   <tr>
                                    <th>NEW PATIENTS</th>
                                    <td><?php echo $patients['total_new'];?></td>
                                </tr>
                                <tr>
                                    <th>RETURNING PATIENTS</th>
                                    <td><?php echo $patients['total_old'];?></td>
                                </tr> -->
                                 <tr>
                                    <th>TOTAL PATIENTS</th>
                                    <td><?php echo $patients8['total_new'] + $patients8['total_old'];?></td>
                                </tr>
                            </tbody>
                        </table>

                        
                        <div class="clearfix"></div>
                    </div>
                    <!-- End Transaction Breakdown -->
                   
                   
                    <div class="col-md-2">
                       
                        <h5>PAYMENT METHODS BREAKDOWN</h5>
                        <table class="table table-striped table-hover table-condensed">
                            <tbody>
                                <?php
                                $total_cash_breakdown = 0;
                                $payment_methods = $this->accounting_model->get_payment_methods();
                                if($payment_methods->num_rows() > 0)
                                {
                                    foreach($payment_methods->result() as $res)
                                    {
                                        $method_name = $res->payment_method;
                                        $payment_method_id = $res->payment_method_id;
                                        $total = $this->accounting_model->get_amount_collected($payment_method_id);
                                 
                                        
                                        echo 
                                        '
                                        <tr>
                                            <th>'.$method_name.'</th>
                                            <td>'.number_format($total, 2).'</td>
                                        </tr>
                                        ';
                                        $total_cash_breakdown += $total;
                                    }
                                    
                                    echo 
                                    '
                                    <tr>
                                        <th>Total</th>
                                        <td>'.number_format($total_cash_breakdown, 2).'</td>
                                    </tr>
                                    ';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                   
                   <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-4 center-align">
                                <h4>CASH INVOICED</h4>
                                <h3>Ksh <?php echo number_format($total_invoices_revenue + $total_debits_revenue -$total_waiver_revenue, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                                 <h4>INSURANCE INVOICED</h4>
                           <h3>Ksh <?php echo number_format($total_invoices_revenue2 + $total_debits_revenue, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                                 <h4>TOTAL INVOICE</h4>
                                <h3>Ksh  <?php echo number_format($total_invoices_revenue + $total_debits_revenue -$total_waiver_revenue, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            
                        </div>
                            <hr>
                         <div class="row">
                            <div class="col-md-4 center-align">
                                   <h4>TODAY'S CASH PAYMENTS</h4>
                                <h3>Ksh <?php echo number_format($total_payments_revenue_current, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                             <h4> TODAY'S INSURANCE PAYMENTS</h4>
                                <h3>Ksh <?php echo number_format($total_payments_revenue, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                                     <h4>TOTAL PAYMENTS</h4>
                                <h3>Ksh <?php echo number_format($total_payments_revenue + $total_payments_revenue_current - $total_waiver_revenue, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            
                        </div>

                       <hr>
                       
                        <hr>
                         <div class="row">
                            <div class="col-md-4 center-align">
                                   <h4>CASH BALANCE</h4>
                            <h3>Ksh <?php echo number_format($total_invoices_revenue + $total_debits_revenue -$total_waiver_revenue - $total_payments_revenue_current , 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                             <h4>INSURANCE BALANCE</h4>
                            <h3>Ksh <?php echo number_format($total_invoices_revenue2 + $total_debits_revenue - $total_payments_revenue , 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            <div class="col-md-4 center-align">
                                     <h4>TOTAL BALANCE</h4>
                                 <h3>Ksh  <?php echo number_format($total_invoices_revenue2 + $total_invoices_revenue + $total_debits_revenue -$total_waiver_revenue - $total_payments_revenue -  $total_payments_revenue_current, 2);?></h3>
                                <!-- <p><?php echo $title;?></p> -->
                            </div>
                            
                        </div>
                       <hr>
                        <div class="row" style="margin-top: 20px;">
                            <div class="center-align">
                                
                              <h4>UPDATE CASH/MPESA PAYMENTS</h4>
                                <h3>Ksh <?php echo number_format($total_payments_revenue2 - $total_payments_revenue_current , 2);?></h3>
                            </div>
                             <div class="center-align">
                                
                              <h4>UPDATE INSURANCE PAYMENTS</h4>
                                <h3>Ksh <?php echo number_format($total_payments_revenue_insurance, 2);?></h3>
                            </div>
                               <div class="center-align">
                                
                              <h4>TOTAL PAYMENTS FOR THE DAY</h4>
                                <h3>Ksh <?php echo number_format($total_payments_revenue_insurance + $total_payments_revenue2, 2);?></h3>
                            </div>
                        </div>


                     
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>